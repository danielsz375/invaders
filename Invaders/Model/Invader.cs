﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Invaders.Model
{
    class Invader : Ship
    {
        public static Size InvaderSize = new Size(25, 15);

        public InvaderType InvaderType { get; private set; }
        public int Score { get; private set; }
        public Invader(InvaderType invaderType, Point location, Size size) 
            : base(location, size)
        {
            InvaderType = invaderType;
        }
        public override void Move(Direction direction)
        {
            Point newLocation = this.Location;
            switch (direction)
            {
                case Direction.Down:
                    newLocation = InvadersModel.CreatePointInNextRow(this.Location);
                    break;
                case Direction.Left:
                    newLocation = new Point(Location.X - 1, Location.Y);
                    break;
                case Direction.Right:
                    newLocation = new Point(Location.X + 1, Location.Y);
                    break;
            }
            this.Location = newLocation;
        }

        public void InvaderShot()
        {
            //TODO
        }
    }
}
