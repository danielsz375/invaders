﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Invaders.Model
{
    class Player : Ship
    {
        public static Size PlayerSize = new Size(25, 15);
        private int _movementSpeed = 10;
        public Player(Point location, Size size) 
            : base(location, size)
        {
        }

        public override void Move(Direction direction)
        {
            Console.WriteLine("PlayerPos: " + Location.X + " " + Location.Y);
            switch(direction)
            {
                case Direction.Left:
                    if (Location.X - _movementSpeed < 0) 
                        return;
                    this.Location = new Point(Location.X - _movementSpeed, Location.Y);
                    break;
                case Direction.Right:
                    if (Location.X - _movementSpeed > 400)
                        return;
                    this.Location = new Point(Location.X + _movementSpeed, Location.Y);
                    break;

            }
        }
    }
}
