﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Invaders.Model
{
    class InvadersModel
    {
        public readonly static Size PlayAreaSize = new Size(400, 330);
        public const int MaximumPlayerShots = 100;
        public const int InitialStarCount = 50;

        private readonly Random _random = new Random();

        public int Score { get; private set; }
        public int Wave { get; private set; }
        public int Lives { get; private set; }

        public bool GameOver { get; private set; }

        private DateTime? _playerDied = null;
        public bool PlayerDying { get { return _playerDied.HasValue; } }

        private Player _player;
        public Player Player { get { return _player; } private set { _player = value; } }

        private readonly List<Invader> _invaders = new List<Invader>();
        private readonly List<Shot> _playerShots = new List<Shot>();
        private readonly List<Shot> _invaderShots = new List<Shot>();
        private readonly List<Point> _stars = new List<Point>();

        private Direction _invaderDirection = Direction.Left;
        private bool _justMovedDown = false;

        private DateTime _lastUpdated = DateTime.MinValue;

        public InvadersModel()
        {
            EndGame();
        }

        public void EndGame()
        {
            GameOver = true;
        }

        public void CreateStars(int amount)
        {
            for (int i = 0; i < amount; i++)
            {
                CreateOneStar();
            }
        }

        private void CreateOneStar()
        {
            int x = _random.Next((int)PlayAreaSize.Width);
            int y = _random.Next((int)PlayAreaSize.Height);

            Point newStar = new Point(x, y);
            _stars.Add(newStar);
            OnStarChanged(newStar, false);
        }

        private void RemoveOneStar()
        {
            if (_stars.Count > 0)
            {
                int index = _random.Next(_stars.Count);
                Point removedStar = _stars[index];
                _stars.RemoveAt(index);
                OnStarChanged(removedStar, true);
            }
        }


        //TESTOWY startPoint
        private Point startPoint = new Point(100, 330);
        public void StartGame()
        {
            GameOver = false;

            _invaders.Clear();
            foreach (Invader invader in _invaders.ToList())
            {
                _invaders.Remove(invader);
                OnShipChanged(invader, true);
            }

            foreach (Shot shot in _playerShots.ToList())
            {
                _playerShots.Remove(shot);
                OnShotMoved(shot, true);
            }

            foreach (Shot shot in _invaderShots.ToList())
            {
                _invaderShots.Remove(shot);
                OnShotMoved(shot, true);
            }

            foreach (Point star in _stars.ToList())
            {
                _stars.Remove(star);
                OnStarChanged(star, true);
            }
            CreateStars(InitialStarCount);

            _player = new Player(startPoint, Player.PlayerSize);
            OnShipChanged(_player, false);
            //ShipChanged
            Lives = 2;
            Wave = 0;
            CreateNewInvadersWave();
        }

        public void FireShot()
        {
            if (_playerShots.Count < MaximumPlayerShots)
            {
                Point shotLocation = new Point(Player.Location.X + Player.PlayerSize.Width / 2 - 1, Player.Location.Y - 10);
                Shot shot = new Shot(shotLocation, Direction.Up);
                _playerShots.Add(shot);
                OnShotMoved(shot, false);
            }
        }

        public void MovePlayer(Direction direction)
        {
            if (!PlayerDying)
            {
                _player.Move(direction);
                OnShipChanged(_player, false);
            }

        }

        public void Twinkle()
        {
            if (_random.Next(2) == 0)
            {
                if ((_stars.Count + 1) < (InitialStarCount * 1.5))
                {
                    CreateOneStar();
                }
            }
            else
            {
                if ((_stars.Count - 1) > (InitialStarCount * 0.15))
                {
                    RemoveOneStar();
                }
            }
        }

        public void Update()
        {
            //Mruganie gwiazdami
            Twinkle();



            //Jeśli gra nie została wstrzymana:
            if (_invaders.Count == 0)
            {
                //Generuj nową falę
                NextWave();
            }
            else if (!PlayerDying)
            {
                //Przesuń statki najeźdźców
                MoveInvaders();
            }

            //Aktualizacja strzałów:
            foreach (Shot shot in _playerShots.ToList())
            {
                shot.Move();

                Point shotLocation = shot.Location;
                Rect shotRect = new Rect(shotLocation, Shot.ShotSize);

                Rect playAreaRect = new Rect(new Point(0, 0), PlayAreaSize);

                if (!RectsOverlap(playAreaRect, shotRect))
                {
                    OnShotMoved(shot, true);
                    _playerShots.Remove(shot);
                }
                else OnShotMoved(shot, false);
            }

            //Najeźdźcy odpowiadają ogniem
            ReturnFire();

            foreach (Shot shot in _invaderShots.ToList())
            {
                shot.Move();

                Point shotLocation = shot.Location;
                Rect shotRect = new Rect(shotLocation, Shot.ShotSize);

                Rect playAreaRect = new Rect(new Point(0, 0), PlayAreaSize);

                if (!RectsOverlap(playAreaRect, shotRect))
                {
                    OnShotMoved(shot, true);
                    _invaderShots.Remove(shot);
                }
                else OnShotMoved(shot, false);
            }

            

            //Sprawdzanie kolizji
            List<Invader> invadersCopy = _invaders.ToList();
            foreach (Invader invader in invadersCopy)
            {
                List<Shot> shotsToRemove = GetShotsThatHitThisInvader(invader);

                if (shotsToRemove.Count > 0)
                {
                    _invaders.Remove(invader);
                    OnShipChanged(invader, true);
                    foreach (Shot shotToRemove in shotsToRemove)
                    {
                        _playerShots.Remove(shotToRemove);
                        OnShotMoved(shotToRemove, true);
                    }
                }
            }

            List<Shot> shotsThatHitPlayer = ShotsThatHitPlayer(); 
            if (shotsThatHitPlayer.Count > 0)
            {
                //śmierć gracza
                _playerDied = DateTime.Now;
                Lives--;
                if(Lives < 0)
                    GameOver = true;

                foreach (Shot shotToRemove in shotsThatHitPlayer)
                {
                    _invaderShots.Remove(shotToRemove);
                    OnShotMoved(shotToRemove, true);
                }
            }

        }

        private List<Shot> ShotsThatHitPlayer()
        {
            var result = from shot in _invaderShots
                         where RectsOverlap(
                             new Rect(shot.Location, Shot.ShotSize),
                             _player.Area)
                         select shot;

            List<Shot> shotList = result.ToList();

            return shotList;
        }

        private List<Shot> GetShotsThatHitThisInvader(Invader invader)
        {
            var result = from shot in _playerShots
                         where RectsOverlap(
                             new Rect(shot.Location, Shot.ShotSize),
                             new Rect(invader.Location, Invader.InvaderSize)
                             )
                         select shot;

            List<Shot> shotList = result.ToList();

            return shotList;
        }

        private void MoveInvaders()
        {
            TimeSpan timeElapsed = DateTime.Now - _lastUpdated;

            //Wartość 100 jest tymczasowa i wymaga korekty
            if (timeElapsed < TimeSpan.FromMilliseconds(1000))
            {
                return;
            }

            if (!_justMovedDown) { 
                if (_invaderDirection == Direction.Right)
                {
                    if (AreInvadersCloseToRightEdge())
                    {
                        MoveInvadersDown();
                        _invaderDirection = Direction.Left;
                        return;
                    }
                
                } 
                else if (_invaderDirection == Direction.Left)
                {
                    if (AreInvadersCloseToLeftEdge())
                    {
                        MoveInvadersDown();
                        _invaderDirection = Direction.Right;
                        return;
                    }
                }
            }


            
            foreach (Invader invader in _invaders)
            {
                invader.Move(_invaderDirection);
                OnShipChanged(invader, false);
            }
            _justMovedDown = false;


        }

        private void MoveInvadersDown()
        {
            foreach (Invader invader in _invaders)
            {
                invader.Move(Direction.Down);
                OnShipChanged(invader, false);
            }
            _justMovedDown = true;
        }

        DateTime? lastShotTime = null;
        TimeSpan shotInterval = TimeSpan.FromMilliseconds(1000);
        private void ReturnFire()
        {

            if (_invaderShots.Count > Wave + 2)
                return;

            if (lastShotTime + shotInterval > DateTime.Now)
                return;

            if (_random.Next(10) > 8 - Wave)
            {
                return;
            }

            
            var invadersAbleToShot = from invader in _invaders
                                 group invader by invader.Location.X
                                 into invaderColumn
                                 select new
                                 {
                                     X = invaderColumn.Key,
                                     Y = invaderColumn.Max(invader => invader.Location.Y)
                                 };

            int randomInvaderIndex = _random.Next(invadersAbleToShot.ToList().Count);

            var shootingInvader = invadersAbleToShot.ToList().ElementAt(randomInvaderIndex);

            //Pozycja musi zostać skorygowana, aby pocisk pojawiał się wyśrodkowany
            Point shotPoint = new Point(shootingInvader.X + Invader.InvaderSize.Width / 2 - 1, shootingInvader.Y + 10);
            Shot shot = new Shot(shotPoint, Direction.Down);

            _invaderShots.Add(shot);
            lastShotTime = DateTime.Now;

            OnShotMoved(shot, false);
        }

        public static bool RectsOverlap(Rect r1, Rect r2)
        {
            r1.Intersect(r2);
            if (r1.Width > 0 || r1.Height > 0)
            {
                return true;
            }
            return false;
        }

        private void NextWave()
        {
            _invaders.Clear();
            CreateNewInvadersWave();
            //nieskończone
        }

        private void CreateNewInvadersWave()
        {
            Wave++;
            Point startRowPoint = new Point(30, 30);
            CreateInvadersRow(startRowPoint, InvaderType.Bug);
            startRowPoint = CreatePointInNextRow(startRowPoint);
            CreateInvadersRow(startRowPoint, InvaderType.Satellite);
            startRowPoint = CreatePointInNextRow(startRowPoint);
            CreateInvadersRow(startRowPoint, InvaderType.Saucer);
            startRowPoint = CreatePointInNextRow(startRowPoint);
            CreateInvadersRow(startRowPoint, InvaderType.Spaceship);
            startRowPoint = CreatePointInNextRow(startRowPoint);
            CreateInvadersRow(startRowPoint, InvaderType.Star);
        }

        private void CreateInvadersRow(Point point, InvaderType invaderType)
        {
            for (int i = 0; i < 10; i++)
            {
                Point invaderPoint = CreateInvaderPoint(point, i);

                Invader invader = new Invader(invaderType, invaderPoint, Invader.InvaderSize);
                _invaders.Add(invader);
                OnShipChanged(invader, false);
            }
        }

        private Point CreateInvaderPoint(Point point, int position)
        {
            double x = point.X + position * 1.4 * Invader.InvaderSize.Width;
            return new Point(x, point.Y);
        }

        public static Point CreatePointToTheLeft(Point point)
        {
            double x = point.X - 1.4 * Invader.InvaderSize.Width;
            return new Point(x, point.Y);
        }

        public static Point CreatePointToTheRight(Point point)
        {
            double x = point.X + 1.4 * Invader.InvaderSize.Width;
            return new Point(x, point.Y);
        }

        public static Point CreatePointInNextRow(Point point)
        {
            double y = point.Y + 1.4 * Invader.InvaderSize.Height;
            return new Point(point.X, y);
        }

        private bool AreInvadersCloseToRightEdge()
        {
            double edgeDistance = PlayAreaSize.Width - Invader.InvaderSize.Width;

            var result = from invader in _invaders
                         where invader.Location.X >= edgeDistance
                         select invader;

            if (result.ToList().Count > 0)
                return true;
            return false;
        }

        private bool AreInvadersCloseToLeftEdge()
        {
            double edgeDistance = Invader.InvaderSize.Width;

            var result = from invader in _invaders
                         where invader.Location.X <= edgeDistance
                         select invader;

            if (result.ToList().Count > 0)
                return true;
            return false;
        }

        public event EventHandler<ShipChangedEventArgs> ShipChanged;

        private void OnShipChanged(Ship ship, bool killed)
        {
            ShipChanged?.Invoke(this, new ShipChangedEventArgs(ship, killed));
        }

        public event EventHandler<ShotMovedEventArgs> ShotMoved;

        private void OnShotMoved(Shot shot, bool disappeared)
        {
            ShotMoved?.Invoke(this, new ShotMovedEventArgs(shot, disappeared));
        }

        public event EventHandler<StarChangedEventArgs> StarChanged;

        private void OnStarChanged(Point point, bool disappeared)
        {
            StarChanged?.Invoke(this, new StarChangedEventArgs(point, disappeared));
        }

        
    }
}
