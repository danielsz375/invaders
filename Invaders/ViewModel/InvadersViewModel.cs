﻿using Invaders.Model;
using Invaders.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Invaders.ViewModel
{
    class InvadersViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private readonly ObservableCollection<UIElement>
            _sprites = new ObservableCollection<UIElement>();

        public INotifyCollectionChanged Sprites { get { return _sprites; } }

        private bool GameOver { get { return _model.GameOver; } }

        private readonly ObservableCollection<object> _lives =
            new ObservableCollection<object>();

        public INotifyCollectionChanged Lives { get { return _lives; } }

        public bool Paused { get; set; }
        private bool _lastPaused = true;

        public static double Scale { get; private set; }
        public int Score { get; private set; }

        public Size PlayAreaSize
        {
            set
            {
                //Scale = value.Width / 405;
                Scale = 1;
                //_model.UpdateAllShipsAndStars();
                _model.Update();
                RecreateScanLines();
            }
        }

        private readonly InvadersModel _model = new InvadersModel();
        private readonly DispatcherTimer _timer = new DispatcherTimer();
        private UIElement _playerControl = null;
        private bool _playerFlashing = true;
        private readonly Dictionary<Invader, UIElement> _invaders =
            new Dictionary<Invader, UIElement>();
        private readonly Dictionary<UIElement, DateTime> _shotInvaders =
            new Dictionary<UIElement, DateTime>();
        private readonly Dictionary<Shot, UIElement> _shots =
            new Dictionary<Shot, UIElement>();
        private readonly Dictionary<Point, UIElement> _stars =
            new Dictionary<Point, UIElement>();
        private readonly List<UIElement> _scanLines =
            new List<UIElement>();

        private DateTime? _leftAction = null;
        private DateTime? _rightAction = null;

        internal void KeyDown(System.Windows.Input.Key inputKey)
        {
            if (inputKey == System.Windows.Input.Key.Space)
            {
                _model.FireShot();
            }

            if (inputKey == System.Windows.Input.Key.Left)
            {
                _leftAction = DateTime.Now;
            }

            if (inputKey == System.Windows.Input.Key.Right)
            {
                _rightAction = DateTime.Now;
            }
        }

        internal void KeyUp(System.Windows.Input.Key inputKey)
        {
            if (inputKey == System.Windows.Input.Key.Left)
            {
                _leftAction = null;
            }

            if (inputKey == System.Windows.Input.Key.Right)
            {
                _rightAction = null;
            }
        }

        public InvadersViewModel()
        {
            _model.ShipChanged += ModelShipChangedEventHandler;
            _model.ShotMoved += ModelShotMovedEventHandler;
            _model.StarChanged += ModelStarChangedEventHandler;
            _timer.Interval = TimeSpan.FromMilliseconds(100);
            _timer.Tick += TimerTickEventHandler;

            EndGame();
            StartGame();
        }

        private void EndGame()
        {
            _model.EndGame();
        }

        public void StartGame()
        {
            Paused = false;
            foreach (var invader in _invaders.Values) _sprites.Remove(invader);
            foreach (var shot in _shots.Values) _sprites.Remove(shot);
            _model.StartGame();
            OnPropertyChanged("GameOver");
            _timer.Start();
        }

        private void RecreateScanLines()
        {
            foreach (UIElement scanLine in _scanLines)
                if (_sprites.Contains(scanLine))
                    _sprites.Remove(scanLine);
            _scanLines.Clear();
            for (int y = 0; y < 300; y += 2)
            {
                UIElement scanLine = InvadersHelper.ScanLineFactory(y, 400, Scale);
                _scanLines.Add(scanLine);
                _sprites.Add(scanLine);
                InvadersHelper.SetCanvasLocation(scanLine, 0, y);
            }
        }

        void TimerTickEventHandler(object sender, object e)
        {
            if (_lastPaused != Paused)
            {
                OnPropertyChanged("Paused");
            }
            if (!Paused)
            {
                if (_leftAction.HasValue && _rightAction.HasValue)
                {
                    if (_leftAction > _rightAction)
                    {
                        _model.MovePlayer(Direction.Left);
                    }
                    else
                    {
                        _model.MovePlayer(Direction.Right);
                    }
                }

                if (_leftAction.HasValue)
                {
                    _model.MovePlayer(Direction.Left);
                }

                if (_rightAction.HasValue)
                {
                    _model.MovePlayer(Direction.Right);
                }
            }

            //TODO: Aktualizacja modelu widoku

            _model.Update();

            if (Score != _model.Score)
            {
                Score = _model.Score;
                OnPropertyChanged("Score");
            }

            //TODO: aktualizacja Lives

            foreach (UIElement shotInvader in _shotInvaders.Keys.ToList())
            {
                if (_shotInvaders[shotInvader] < DateTime.Now - TimeSpan.FromSeconds(0.5))
                {
                    _sprites.Remove(shotInvader);
                    _shotInvaders.Remove(shotInvader);
                }
            }

            if (GameOver)
            {
                OnPropertyChanged("GameOver");
                _timer.Stop();
            }
        }

        void ModelShipChangedEventHandler(object sender, ShipChangedEventArgs e)
        {
            if (!e.Killed)
            {
                if (e.ShipUpdated is Invader)
                {
                    Invader invader = e.ShipUpdated as Invader;
                    UIElement invaderControl;
                    if (!_invaders.ContainsKey(invader))
                    {
                        invaderControl = InvadersHelper.InvaderControlFactory(invader.InvaderType, Invader.InvaderSize, Scale);
                        _invaders[invader] = invaderControl;
                        _sprites.Add(invaderControl);
                        InvadersHelper.SetCanvasLocation(invaderControl, invader.Location.X, invader.Location.Y);
                    } 
                    else
                    {
                        invaderControl = _invaders[invader] as AnimatedImage;
                        InvadersHelper.MoveShipControl(invaderControl, invader.Location.X, invader.Location.Y);

                        InvadersHelper.ResizeElement(invaderControl as AnimatedImage, invader.Size.Width * Scale,
                            invader.Size.Height * Scale);
                    }
                    InvadersHelper.SetCanvasLocation(invaderControl, invader.Location.X, invader.Location.Y);
                } else if (e.ShipUpdated is Player)
                {
                    Player player = _model.Player;
                    if (_playerFlashing)
                    {
                        //Przerwanie migotania:
                        _playerFlashing = false;
                        if (_playerControl == null)
                        {
                            _playerControl = InvadersHelper.PlayerControlFactory(Player.PlayerSize, Scale);
                            _sprites.Add(_playerControl);
                            InvadersHelper.SetCanvasLocation(_playerControl, player.Location.X, player.Location.Y);
                        } 
                    }
                    else
                    {
                        //Przesuń statek gracza:
                        //_playerControl.Move();

                        InvadersHelper.MoveShipControl(_playerControl, player.Location.X, player.Location.Y);

                        InvadersHelper.ResizeElement(_playerControl as AnimatedImage, player.Size.Width * Scale,
                        player.Size.Height * Scale);
                    }
                    //InvadersHelper.SetCanvasLocation(_playerControl, _model.Player.Location.X, _model.Player.Location.Y);
                }

            }
            else
            {
                if (e.ShipUpdated is Invader)
                {
                    Invader invader = e.ShipUpdated as Invader;
                    if (invader != null)
                    {
                        invader.InvaderShot();
                        UIElement invaderControl = _invaders[invader];
                        _shotInvaders.Add(invaderControl, DateTime.Now);
                        //TODO: Obiekt  modelu widoku  nie  usuwa  kontrolki  Animatedlmage  reprezentującej  statek najeźdźcy  aż  do momentu,  gdy  zakończy  on  migotać.
                    }
                    else if (e.ShipUpdated is Player)
                    {
                        AnimatedImage playerControl = _playerControl as AnimatedImage;
                        _playerFlashing = true;
                        //Możliwe, że trzeba tu coś jeszcze dodać
                    }
                }
            }
        }

        void ModelShotMovedEventHandler(object sender, ShotMovedEventArgs e)
        {
            if (!e.Disappeared)
            {
                if (!_shots.ContainsKey(e.Shot))
                {
                    UIElement shotControl = InvadersHelper.ShotControlFactory(Shot.ShotSize, Scale);
                    _shots[e.Shot] = shotControl;
                    _sprites.Add(shotControl);
                    InvadersHelper.SetCanvasLocation(shotControl, e.Shot.Location.X, e.Shot.Location.Y);
                } 
                else
                {
                    UIElement shotControl = _shots[e.Shot];
                    InvadersHelper.MoveShotControl(shotControl, e.Shot.Location.X, e.Shot.Location.Y);
                }
            }
            else
            {
                if (_shots.ContainsKey(e.Shot))
                {
                    UIElement shotControl = _shots[e.Shot];
                    _sprites.Remove(shotControl);
                    _shots.Remove(e.Shot);
                }
            }
        }

        void ModelStarChangedEventHandler(object sender, StarChangedEventArgs e)
        {
            if (e.Disappeared && _stars.ContainsKey(e.Point))
            {
                UIElement starControl = _stars[e.Point];
                _stars.Remove(e.Point);
                _sprites.Remove(starControl);
            }
            else
            {
                if (!_stars.ContainsKey(e.Point))
                {
                    //AnimatUIedImage starControl = InvadersHelper.StarControlFactory() as AnimatedImage;
                    UIElement starControl = InvadersHelper.StarControlFactory(Scale);
                    _stars[e.Point] = starControl;
                    _sprites.Add(starControl);
                    InvadersHelper.SetCanvasLocation(starControl, e.Point.X, e.Point.Y);
                } 
                else
                {
                    //Można tu zrobić spadające gwiazdy
                }
            }
        }
    }
}
