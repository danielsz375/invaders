﻿using Invaders.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Invaders.View
{
    /// <summary>
    /// Logika interakcji dla klasy InvadersMainWindow.xaml
    /// </summary>
    public partial class InvadersMainWindow : Window
    {
        InvadersViewModel viewModel;
        public InvadersMainWindow()
        {
            InitializeComponent();

            viewModel = FindResource("viewModel") as InvadersViewModel;
        }

        private void mainWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdatePlayAreaSize(new Size(e.NewSize.Width, e.NewSize.Height - 160));
        }

        private void playArea_Loaded(object sender, RoutedEventArgs e)
        {
            UpdatePlayAreaSize(playArea.RenderSize);
        }

        private void UpdatePlayAreaSize(Size newPlayAreaSize)
        {
            double targetWidth;
            double targetHeight;

            if (newPlayAreaSize.Width > newPlayAreaSize.Height)
            {
                targetWidth = newPlayAreaSize.Height * 4 / 3;
                targetHeight = newPlayAreaSize.Height;
                double leftRightMargin = (newPlayAreaSize.Width - targetWidth) / 2;
                playArea.Margin = new Thickness(leftRightMargin, 0, leftRightMargin, 0);
            }
            else
            {
                targetHeight = newPlayAreaSize.Width * 3 / 4;
                targetWidth = newPlayAreaSize.Width;
                double topBottomMargin = (newPlayAreaSize.Height - targetHeight) / 2;
                playArea.Margin = new Thickness(0, topBottomMargin, 0, topBottomMargin);
            }

            playArea.Width = targetWidth;
            playArea.Height = targetHeight;
            viewModel.PlayAreaSize = playArea.RenderSize;
        }

        //W OnNavigated... było override, ale powodowało błąd
        protected void OnNavigatedTo(NavigationEventArgs e)
        {
            this.KeyDown += KeyDownHandler;
            this.KeyUp += KeyUpHandler;
            //base.OnNavigatedTo(e);
        }

        protected void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.KeyDown -= KeyDownHandler;
            this.KeyUp -= KeyUpHandler;
            //base.OnNavigatedFrom(e);
        }

        private void KeyDownHandler(object sender, KeyEventArgs e)
        {
            Console.WriteLine("KeyDown: " + e.Key);
            viewModel.KeyDown(e.Key);
        }

        private void KeyUpHandler(object sender, KeyEventArgs e)
        {
            viewModel.KeyUp(e.Key);
        }
    }
}
