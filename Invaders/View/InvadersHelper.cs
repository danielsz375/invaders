﻿using Invaders.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Invaders.View
{
    static class InvadersHelper
    {
        private readonly static Random _random = new Random();
        private static string _directory = @"C:\Users\Takedown\source\repos\Invaders\Invaders\Assets\";
        public static UIElement StarControlFactory(double scale = 1)
        {
            int number =_random.Next(3);
            UIElement element;
            int size;
            switch (number)
            {
                case 0:
                    Rectangle rectangle = new Rectangle();
                    rectangle.Fill = RandomColorFactory();
                    size = 1;
                    rectangle.Width = size * scale;
                    rectangle.Height = size * scale;
                    element = rectangle;
                    break;
                case 1:
                    Ellipse ellipse = new Ellipse();
                    ellipse.Fill = RandomColorFactory();
                    size = _random.Next(2, 3);
                    ellipse.Width = size * scale;
                    ellipse.Height = size * scale;
                    element = ellipse;
                    break;
                case 2:
                default:
                    BigStarControl bigStarControl = new BigStarControl();
                    bigStarControl.SetFill(RandomColorFactory());
                    element = bigStarControl;
                    break;
            }



            return element;
        }

        public static AnimatedImage PlayerControlFactory(Size size, double scale = 1)
        {
            List<string> imageNames = new List<string>();

            imageNames.Add(_directory + "player.png");

            AnimatedImage playerControl = new AnimatedImage(imageNames, TimeSpan.FromMilliseconds(1000));
            playerControl.Width = size.Width * scale;
            playerControl.Height = size.Height * scale;

            return playerControl;
        }

        public static AnimatedImage InvaderControlFactory(InvaderType invaderType, Size size, double scale = 1)
        {
            List<string> imageNames = new List<string>();
            switch (invaderType)
            {
                case InvaderType.Bug:
                    imageNames.Add(_directory + "bug1.png");
                    imageNames.Add(_directory + "bug2.png");
                    imageNames.Add(_directory + "bug3.png");
                    imageNames.Add(_directory + "bug4.png");
                    break;
                case InvaderType.Saucer:
                    imageNames.Add(_directory + "flyingsaucer1.png");
                    imageNames.Add(_directory + "flyingsaucer2.png");
                    imageNames.Add(_directory + "flyingsaucer3.png");
                    imageNames.Add(_directory + "flyingsaucer4.png");
                    break;
                case InvaderType.Satellite:
                    imageNames.Add(_directory + "satellite1.png");
                    imageNames.Add(_directory + "satellite2.png");
                    imageNames.Add(_directory + "satellite3.png");
                    imageNames.Add(_directory + "satellite4.png");
                    break;
                case InvaderType.Spaceship:
                    imageNames.Add(_directory + "spaceship1.png");
                    imageNames.Add(_directory + "spaceship2.png");
                    imageNames.Add(_directory + "spaceship3.png");
                    imageNames.Add(_directory + "spaceship4.png");
                    break;
                case InvaderType.Star:
                default:
                    imageNames.Add(_directory + "star1.png");
                    imageNames.Add(_directory + "star2.png");
                    imageNames.Add(_directory + "star3.png");
                    imageNames.Add(_directory + "star4.png");
                    break;
            }

            AnimatedImage invaderControl = new AnimatedImage(imageNames, TimeSpan.FromMilliseconds(500));
            invaderControl.Width = size.Width * scale;
            invaderControl.Height = size.Height * scale;


            return invaderControl;
        }

        public static UIElement ShotControlFactory(Size size, double scale = 1)
        {

            Rectangle shotControl = new Rectangle();
            shotControl.Fill = new SolidColorBrush(Colors.Yellow);

            shotControl.Width = 2 * scale; ;
            shotControl.Height = 10 * scale;

            //ShotControl shotControl = new ShotControl();
            //shotControl.Width = size.Width * scale;
            //shotControl.Height = size.Height * scale;
                        

            return shotControl;
        }

        private static SolidColorBrush RandomColorFactory()
        {
            List<Color> colors = new List<Color>();
            colors.Add(Colors.Blue);
            colors.Add(Colors.Red);
            colors.Add(Colors.Green);
            Color randomColor = colors[_random.Next(colors.Count)];
            
            return new SolidColorBrush(randomColor);
        }

        public static UIElement ScanLineFactory(double y, double x, double scale)
        {
            Rectangle scanLine = new Rectangle();
            scanLine.Fill = new SolidColorBrush(Colors.White);
            scanLine.Height = 2 * scale;
            scanLine.Opacity = 1;
            return scanLine;
        }

        public static UIElement ShotFactory(double scale = 1)
        {
            Rectangle shot = new Rectangle();

            shot.Width = Shot.ShotSize.Width * scale;
            shot.Height = Shot.ShotSize.Height * scale;
            shot.Fill = new SolidColorBrush(Colors.Yellow);

            return shot;
        }

        public static void ResizeElement(AnimatedImage animatedImage, double newWidth, double newHeight)
        {
            animatedImage.Width = newWidth;
            animatedImage.Height = newHeight;
        }

        public static void MoveShotControl(UIElement shotControl, double toX, double toY)
        {
            MoveElementOnCanvas(shotControl, toX, toY);
            //SetCanvasLocation(shotControl, toX, toY);
        }

        public static void MoveShipControl(UIElement shipControl, double toX, double toY)
        {
            MoveElementOnCanvas(shipControl, toX, toY);
            //SetCanvasLocation(shipControl, toX, toY);
        }

        public static void SetCanvasLocation(UIElement control, double x, double y)
        {
            Canvas.SetLeft(control, x);
            Canvas.SetTop(control, y);
        }

        public static void MoveElementOnCanvas(UIElement uiElement, double toX, double toY)
        {
            double fromX = Canvas.GetLeft(uiElement);
            double fromY = Canvas.GetTop(uiElement);

            Storyboard storyboard = new Storyboard();
            DoubleAnimation animationX = CreateDoubleAnimation(uiElement,
                fromX, toX, new PropertyPath(Canvas.LeftProperty));
            DoubleAnimation animationY = CreateDoubleAnimation(uiElement,
                fromY, toY, new PropertyPath(Canvas.TopProperty));

            storyboard.Children.Add(animationX);
            storyboard.Children.Add(animationY);
            storyboard.Begin();
        }

        public static DoubleAnimation CreateDoubleAnimation(UIElement uiElement,
            double from, double to, PropertyPath propertyToAnimate)
        {
            DoubleAnimation animation = new DoubleAnimation();
            Storyboard.SetTarget(animation, uiElement);
            Storyboard.SetTargetProperty(animation, propertyToAnimate);
            animation.From = from;
            animation.To = to;
            animation.Duration = TimeSpan.FromMilliseconds(250);
            return animation;
        }
    }
}
